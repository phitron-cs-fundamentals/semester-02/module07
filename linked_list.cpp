#include <bits/stdc++.h>
using namespace std;
class Node
{
public:
    int val;
    Node *next;
    Node(int val) // constructor creating
    {
        this->val = val;
        this->next = NULL;
    }
};

void print_linked_list(Node *head)
{
    Node *tmp = head;
    while (tmp != NULL)
    {
        /* code */
        cout << tmp->val << " ";
        tmp = tmp->next;
    }
}

int main()
{
    // Node a, b;  //Old method
    // a.val = 20;
    // b.val = 30;
    // a.next = &b;
    Node *head = new Node(5);
    Node *a = new Node(10);
    Node *b = new Node(20);
    Node *c = new Node(30);
    Node *d = new Node(40);
    head->next = a;
    a->next = b; // next method
    b->next = c;
    c->next = d;
    print_linked_list(head);

    // cout << (*a).val << endl; // print a
    // cout << a->val << endl; //print a
    // cout<<a->next->val<<endl; //print b for next method
    
    // cout << a.val << endl; //Old Method
    return 0;
}