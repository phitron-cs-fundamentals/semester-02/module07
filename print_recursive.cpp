#include <bits/stdc++.h>
using namespace std;
class Node
{
public:
    int val;
    Node *next;
    Node(int val) // constructor creating
    {
        this->val = val;
        this->next = NULL;
    }
};

void print_linked_list(Node *head)
{
    Node *tmp = head;
    while (tmp != NULL)
    {
        /* code */
        cout << tmp->val << endl;
        tmp = tmp->next;
    }
}

void insert(Node *head, int pos, int val)
{
    Node *newNode = new Node(val);
    Node *tmp = head;
    for (int i = 1; i <= pos; i++)
    {
        tmp = tmp->next;
    }
    // cout<<tmp->val<<endl; //tmp = pos-1
    newNode->next = tmp->next;
    tmp->next = newNode;
}

int size(Node *head)
{
    Node *tmp = head;
    int count = 0;
    while (tmp != NULL)
    {
        /* code */
        count++;
        tmp = tmp->next;
    }
    return count;
}

void insert_head(Node *&head, int val)
{
    Node *newNode = new Node(val);
    newNode->next = head;
    head = newNode;
}

void insert_tail(Node *head, Node *tail, Node val)
{
    Node *newNode = new Node(val);
    if (head == NULL)
    {
        head = newNode;
        tail = newNode;
        return;
    }
    tail->next = newNode;
    tail = newNode;
}
void print_recursion(Node *n)
{
    if (n == NULL)
        return;
    cout << n->val << " ";
    print_recursion(n->next);
}

void print_reverse(Node *n)
{
    if (n == NULL)
        return;

    print_reverse(n->next);
    cout << n->val << " ";
}
int main()
{
    Node *head = new Node(5);
    Node *a = new Node(10);
    Node *b = new Node(20);
    Node *c = new Node(30);
    Node *d = new Node(40);
    head->next = a;
    a->next = b; // next method
    b->next = c;
    c->next = d;

    print_recursion(head);cout<<endl;
    print_reverse(head);

    return 0;
}